package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.constant.ArgumentConst;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.model.Command;

public class CommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConstant.ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info."
    );
    public static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConst.ARG_VERSION, "Display program version."
    );
    public static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands."
    );public static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConst.ARG_INFO, "Display system information."
    );
    public static final Command EXIT = new Command(
            TerminalConstant.EXIT, null, "Close application."
    );
    public static final Command[] COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }

}
