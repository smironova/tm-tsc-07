package com.tsc.smironova.tm.model;

public class Command {

    private final String name;
    private final String argument;
    private final String description;

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getArgument() {
        return argument;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        String result = "";
        if (this.name != null && !this.name.isEmpty())
            result += this.name + " ";
        if (this.argument != null && !this.argument.isEmpty())
            result += "(" + this.argument + ") ";
        if (this.description != null && !this.description.isEmpty())
            result += "- " + this.description + " ";
        return result;
    }

}
